import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartLineHiresbyyearComponent } from './chart-line-hiresbyyear.component';

describe('ChartLineHiresbyyearComponent', () => {
  let component: ChartLineHiresbyyearComponent;
  let fixture: ComponentFixture<ChartLineHiresbyyearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartLineHiresbyyearComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartLineHiresbyyearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
