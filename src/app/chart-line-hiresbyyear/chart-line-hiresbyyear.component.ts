import { Component, OnInit } from '@angular/core';
import { GenericLineComponent } from '../chart-generic/chart-line-generic.component';

@Component({
  selector: 'app-chart-line-hiresbyyear',
  templateUrl: './chart-line-hiresbyyear.component.html',
  styleUrls: ['./chart-line-hiresbyyear.component.css']
})
export class ChartLineHiresbyyearComponent extends GenericLineComponent implements OnInit {

  constructor() {
      super('Year');
  }

  ngOnInit() {
  }

  accumulate(value: number): number {
      return value ? (value + 1) : 1;
  }

  accumulateThreshold(value: number): number {
      return value + 1;
  }

}
