import { Component, OnInit, Input } from '@angular/core';
import { GenericPieComponent } from '../chart-generic/chart-pie-generic.component';

@Component({
  selector: 'app-chart-pie-skillcount',
  templateUrl: './chart-pie-skillcount.component.html',
  styleUrls: ['./chart-pie-skillcount.component.css']
})
export class ChartPieSkillcountComponent extends GenericPieComponent implements OnInit {

  constructor() {
    super('Skill');
  }

  ngOnInit() {
  }

  accumulate(count: number): number {
    return count ? (count + 1) : 1;
  }

  accumulateThreshold(othersCount: number): number {
    return othersCount + 1;
  }

}
