import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartPieSkillcountComponent } from './chart-pie-skillcount.component';

describe('ChartPieSkillcountComponent', () => {
  let component: ChartPieSkillcountComponent;
  let fixture: ComponentFixture<ChartPieSkillcountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartPieSkillcountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartPieSkillcountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
