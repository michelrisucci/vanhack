import { OnInit, Input } from '@angular/core';
import { IVanhackReportData } from '../services/datasource.service';
import { ILineChartData } from './chart-line-generic-data';

export abstract class GenericLineComponent implements OnInit {

    protected _data: IVanhackReportData[];
    protected _chartData: ILineChartData[];

    constructor(private propertyName: string) { }

    ngOnInit() {
    }

    abstract accumulate(value: number): number;

    abstract accumulateThreshold(value: number): number;

    protected transform(data: IVanhackReportData[]): ILineChartData[] {
        let line = {
            name: "Year Success Cases",
            series: []
        };

        let result = new Array<ILineChartData>();
        result.push(line);

        if (data) {

            // Declaring accumulator map
            const accumulator = new Map<string, number>();

            // Accumulating
            data.forEach(element => {
                const key = String(element[this.propertyName]).trim();
                if (key) {
                    const value = accumulator.get(key);
                    accumulator.set(key, this.accumulate(value));
                }
            });

            // Processing accumulated values to the Pie Chart data
            let othersCount = 0;
            Array.from(accumulator.keys()).forEach(element => {
                const value = accumulator.get(element);
                line.series.push({
                    name: element,
                    value: value
                });
            });
        }

        // Returning processed data
        return result;
    }

    get data(): IVanhackReportData[] {
        return this._data;
    }

    @Input()
    set data(value: IVanhackReportData[]) {
        this._data = value;
        this._chartData = this.transform(this._data);
    }

    get chartData(): ILineChartData[] {
        return this._chartData;
    }

    set chartData(value: ILineChartData[]) {
        this._chartData = value;
    }

}
