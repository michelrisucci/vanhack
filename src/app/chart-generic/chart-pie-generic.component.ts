import { OnInit, Input } from '@angular/core';
import { IVanhackReportData } from '../services/datasource.service';
import { IChartData } from './chart-generic-data';

export abstract class GenericPieComponent implements OnInit {

    protected _data: IVanhackReportData[];
    protected _chartData: IChartData[];

    constructor(private propertyName: string, private maxItems: number = 8) {}

    ngOnInit() {
    }
    
    abstract accumulate(value: number): number;

    abstract accumulateThreshold(value: number): number;

    protected transform(data: IVanhackReportData[]): IChartData[] {
        let result = new Array<IChartData>();
            if (data) {

            // Declaring accumulator map
            const accumulator = new Map<string, number>();
            
            // Accumulating
            data.forEach(element => {
                const key = element[this.propertyName].trim();
                if (key) {
                    const value = accumulator.get(key);
                    accumulator.set(key, this.accumulate(value));
                }
            });

            // Sorting values to discover threshold value
            const accumulatedValues = Array.from(accumulator.values());
            accumulatedValues.sort((v1, v2) => v2 - v1);

            // Discovering threshold value (max number of items shown on map)
            let threshold = accumulatedValues.length - 1;
            if (threshold > this.maxItems) {
                threshold = this.maxItems;
            }
            const thresholdValue = accumulatedValues[threshold];

            // Processing accumulated values to the Pie Chart data
            let othersCount = 0;
            Array.from(accumulator.keys()).forEach(element => {
                const value = accumulator.get(element);
                if (value < thresholdValue) {
                    accumulator.delete(element);
                    othersCount += this.accumulateThreshold(value);
                } else {
                    result.push({
                        name: element,
                        value: value
                    });
                }
            });

            // Collecting grouped 'Other' values
            result.push({
                name: 'Other',
                value: othersCount
            });
            result = result.filter(item => item.value >= thresholdValue);
        }

        // Returning processed data
        return result;
    }

    get data(): IVanhackReportData[] {
        return this._data;
    }

    @Input()
    set data(value: IVanhackReportData[]) {
        this._data = value;
        this._chartData = this.transform(this._data);
    }

    get chartData(): IChartData[] {
        return this._chartData;
    }
    
    set chartData(value: IChartData[]) {
        this._chartData = value;
    }
    
}
