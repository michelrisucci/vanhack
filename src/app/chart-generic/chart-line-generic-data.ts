import { IChartData } from "./chart-generic-data";

export interface ILineChartData {
  name?: string;
  series?: IChartData[]
}
