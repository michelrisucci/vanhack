import { Component, OnInit } from '@angular/core';
import { DatasourceService, IVanhackReportData } from './services/datasource.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private title = 'Vanhack';

  private data: IVanhackReportData[];

  constructor(private datasourceService: DatasourceService) {}

  ngOnInit() {
    this.datasourceService.fetch().subscribe(
      data => {
        this.data = data.body;
        console.log(this.data);
      },
      error => console.log(error)
    );
  }

}
