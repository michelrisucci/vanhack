import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

import { AppComponent } from './app.component';
import { ChartPieSkillcountComponent } from './chart-pie-skillcount/chart-pie-skillcount.component';
import { ChartPieCitiesshareComponent } from './chart-pie-citiesshare/chart-pie-citiesshare.component';
import { ChartLineHiresbyyearComponent } from './chart-line-hiresbyyear/chart-line-hiresbyyear.component';
import { ChartMapCitiesshareComponent } from './chart-map-citiesshare/chart-map-citiesshare.component';
import { TableDatasourceComponent } from './table-datasource/table-datasource.component';

@NgModule({
  declarations: [
    AppComponent,
    ChartPieSkillcountComponent,
    ChartPieCitiesshareComponent,
    ChartLineHiresbyyearComponent,
    ChartMapCitiesshareComponent,
    TableDatasourceComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgbModule,
    NgxChartsModule,
    LeafletModule.forRoot()
  ],
  providers: [],
  bootstrap: [ AppComponent ],
  schemas: []
})
export class AppModule { }
