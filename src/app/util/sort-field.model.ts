export enum SortDirection {
    ASC, DESC
}

export interface ISortField {
  name?: string;
  direction?: SortDirection;
}

export class SortField implements ISortField {
  constructor(name: string, direction: SortDirection) {}
}
