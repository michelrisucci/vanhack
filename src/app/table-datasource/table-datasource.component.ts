import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { IVanhackReportData } from '../services/datasource.service';

@Component({
  selector: 'app-table-datasource',
  templateUrl: './table-datasource.component.html',
  styleUrls: ['./table-datasource.component.css']
})
export class TableDatasourceComponent implements OnInit, OnChanges {

  @Input()
  private data: IVanhackReportData[];
  private sorted = false;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
  }

}
