import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartPieCitiesshareComponent } from './chart-pie-citiesshare.component';

describe('ChartPieCitiesshareComponent', () => {
  let component: ChartPieCitiesshareComponent;
  let fixture: ComponentFixture<ChartPieCitiesshareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartPieCitiesshareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartPieCitiesshareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
