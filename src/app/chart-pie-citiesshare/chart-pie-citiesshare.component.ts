import { Component, OnInit, Input } from '@angular/core';
import { GenericPieComponent } from '../chart-generic/chart-pie-generic.component';

@Component({
  selector: 'app-chart-pie-citiesshare',
  templateUrl: './chart-pie-citiesshare.component.html',
  styleUrls: ['./chart-pie-citiesshare.component.css']
})
export class ChartPieCitiesshareComponent extends GenericPieComponent implements OnInit {

  constructor() {
    super('City');
  }

  ngOnInit() {
  }

  accumulate(value: number): number {
    return value ? (value + 1) : 1;
  }

  accumulateThreshold(value: number): number {
    return value + 1;
  }

}
