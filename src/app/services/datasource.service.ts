import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';

export interface IVanhackReportData {
  Name?: string;
  City?: string;
  Area?: string;
  Skill?: string;
  Year?: number;
}

@Injectable({
  providedIn: 'root'
})
export class DatasourceService {

  private static API = 'https://gitlab.com/michelrisucci/vanhack/raw/master';

  constructor(private  httpClient:  HttpClient) { }

  public fetch(): Observable<HttpResponse<IVanhackReportData[]>> {
    return this.httpClient.get<IVanhackReportData[]>(DatasourceService.API + '/data.json', { observe: 'response' });
  }

}
