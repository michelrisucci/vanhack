import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartMapCitiesshareComponent } from './chart-map-citiesshare.component';

describe('ChartMapCitiesshareComponent', () => {
  let component: ChartMapCitiesshareComponent;
  let fixture: ComponentFixture<ChartMapCitiesshareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartMapCitiesshareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartMapCitiesshareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
