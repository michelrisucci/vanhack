import { Component, OnInit } from '@angular/core';
import { latLng, tileLayer, marker, icon } from 'leaflet';

import { IVanhackReportData } from '../services/datasource.service';

@Component({
  selector: 'app-chart-map-citiesshare',
  templateUrl: './chart-map-citiesshare.component.html',
  styleUrls: ['./chart-map-citiesshare.component.css']
})
export class ChartMapCitiesshareComponent implements OnInit {
  
  private data: IVanhackReportData[];
  // Define our base layers so we can reference them multiple times
  private streetMaps = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    detectRetina: true,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  });
  private wMaps = tileLayer('http://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
    detectRetina: true,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  });
  private options = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { minZoom: 1, maxZoom: 3 }),
      
    ],
    zoom: 2,
    center: latLng(20, -20)
  };
  private layersControl;

  constructor() { }

  ngOnInit() {
    this.layersControl = {
      // baseLayers: {
      //   'Open Street Map': tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18 }),
      //   'Open Cycle Map': tileLayer('https://{s}.tile.opencyclemap.org/{z}/{x}/{y}.png', { maxZoom: 18 })
      // },
      overlays: {
        'Marker': marker([ 46.78465227596462,-121.74141269177198 ], {
          icon: icon({
            iconSize: [ 25, 41 ],
            iconAnchor: [ 13, 41 ],
            iconUrl: 'node_modules/leaflet/dist/images/marker-icon.png',
            shadowUrl: 'node_modules/leaflet/dist/images/marker-shadow.png'
          })
        })
      }
    }
  }

}
